

import fs from 'fs'
import path from 'path'


const create_symlink = (from, to) => {
  try {
    fs.unlinkSync(to)
  } catch (e) {

  }
  fs.symlinkSync(from, to);
}


class DirLevelWatch {
  constructor(src, dst, valid) {
    this.src = src;
    this.dst = dst;

    this.files_to_update = [];

    const _this = this;
    fs.watch(src, (evt_type, filename) => {
      if (valid.includes(filename)) _this.queue_file(filename);
    });

    setInterval(() => {
      _this.update();
    }, 100);
  }

  update() {
    for (let filename of this.files_to_update) {
      const src_path = path.resolve(this.src, filename);
      const dst_path = path.resolve(this.dst, filename);
      console.log(`[CHANGE] >> SYNC
      src: ${src_path}
      dst: ${dst_path}`);
      fs.copyFileSync(src_path, dst_path);
    }
    this.files_to_update = [];
  }

  queue_file(fname) {
    if (!this.files_to_update.includes(fname)) {
      this.files_to_update.push(fname);
    }
  }
}

class DirSync {
  constructor(src, dst) {
    console.log(`Sync directories:
      src: ${src}
      dst: ${dst}`);
    this.src = src;
    this.dst = dst;

    const files_to_watch = [];
    const sync_level = (src, dst) => {
      fs.mkdirSync(dst);
      const dir_files = fs.readdirSync(src);
      for (const file of dir_files) {
        const file_path = path.resolve(src, file);
        const file_path_dest = path.resolve(dst, file);
        if (fs.lstatSync(file_path).isDirectory()) {
          sync_level(file_path, file_path_dest);
        } else {
          fs.copyFileSync(file_path, file_path_dest)
          files_to_watch.push(file);
        }
      }

      new DirLevelWatch(src, dst, files_to_watch);
    }
    sync_level(src, dst);

  }


}

export default class FlyPack {
  static async run(config) {

    const CWD = process.cwd();


    const zunzun_json_path = path.resolve(CWD, '.zunzun.yaml');
    const zunzun_twigs_path = path.resolve(CWD, 'twigs');
    const node_modules_path = path.resolve(CWD, 'node_modules');

    const zunzun_twigs_lib_path = path.resolve(zunzun_twigs_path, 'lib');
    const zunzun_twigs_lib_link = path.resolve(node_modules_path, 'zunzun');

    const zunzun_top_srv_path = path.resolve(CWD, 'service');
    const zunzun_twigs_srv_path = path.resolve(zunzun_twigs_path, 'srv');

    const zunzun_top_lib_path = path.resolve(CWD, 'library');
    const zunzun_top_lib_link = path.resolve(zunzun_twigs_lib_path, 'local/');


    const runtime_dir = path.resolve(CWD, '.zun');
    if (!fs.existsSync(runtime_dir)) fs.mkdirSync(runtime_dir);
    const runtime_srv_dir = path.resolve(runtime_dir, 'srv');
    if (!fs.existsSync(runtime_srv_dir)) fs.mkdirSync(runtime_srv_dir);

    if (!fs.existsSync(zunzun_twigs_lib_link)) fs.mkdirSync(zunzun_twigs_lib_link);

    const local_libs = fs.readdirSync(zunzun_twigs_lib_path);
        console.log("LINK TO TWIG", local_libs);

    for (let ldir of local_libs) {
      let from = path.resolve(zunzun_twigs_lib_path, ldir);
      let to = path.resolve(zunzun_twigs_lib_link, ldir);

      if (fs.lstatSync(from).isSymbolicLink()) {
        from = fs.readlinkSync(from)

        try { fs.rmSync(to, { recursive: true }) } catch (e) { }

        new DirSync(from, to)

      } else {
        create_symlink(from, to);
      }
    }


    const local_srvs = fs.readdirSync(zunzun_twigs_srv_path);

    for (let sdir of local_srvs) {
      let from = path.resolve(zunzun_twigs_srv_path, sdir);
      let to = path.resolve(runtime_srv_dir, sdir);

      if (fs.lstatSync(from).isSymbolicLink()) {
        from = fs.readlinkSync(from)

        try { fs.rmSync(to, { recursive: true }) } catch (e) { }

        new DirSync(from, to)

      } else {
        create_symlink(from, to);
      }
    }
  }

}
